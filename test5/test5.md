# 实验5：包，过程，函数的用法

- 学号：202010414125
- 姓名：叶攀
- 班级：1

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 实验步骤

### 1.登录hr用户，创建MyPack包

```sql
create or replace PACKAGE MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
```

![](./create_pack.jpg)

### 2.创建函数Get_SalaryAmount。

```sql
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); 
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;
```



### 3.在MyPack中创建过程GET_EMPLOYEES

```sql
PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```

![](./create_fun_pro.jpg)



### 4.测试

测试函数Get_SalaryAmount()：

```sql
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
```

![](./test_fun.png)

测试过程Get_Employees()：

```sql
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/
```

![](./test_pro.png)



## 实验总结

本次实验中，我学习到了如何创建package和在包中创建函数和过程。

```sql
create or replace package package_name --创建包头，定义包的对象名称，如果存在就替换包

is/as   --在这里is和as是同义词，没有区别

--声明变量

--声明常量

--类型的定义

--游标的定义

--声明存储过程

procedure procedure_name[(参数 in | out | in out 类型,...)];

--声明自定义函数（  ？）

function function_name[(参数 类型,...)] return 返回值类型;
```
由此次实验可知，在PL/SQL中，变量和常量是非常重要的概念，可以通过DECLARE关键字来声明，并且需要指定变量或常量的数据类型。

包、过程、函数是PL/SQL中比较重要的三个概念。包是一种将相关对象组织在一起的方式，可以将多个过程和函数放在同一个包中；过程和函数则分别对应着不返回值和返回值的代码块，在使用时可以通过CALL关键字来调用。
