# 实验4：PL/SQL语言打印杨辉三角

- 学号：202010414125
- 姓名：叶攀
- 班级：1

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriangle的SQL语句。

## 实验步骤

### 1.建立杨辉三角存储过程

```sql
create or replace PROCEDURE    YHTriangle(N IN NUMBER) AS TYPE 
t_number IS VARRAY(100) OF INTEGER NOT NULL; --定义VARRAY类型的数组 i INTEGER; 
j INTEGER; spaces VARCHAR2(30) :=' '; --三个空格，用于打印时分隔数字 
rowArray t_number := t_number(); --声明rowArray为t_number类型的数组并初始化为空 
BEGIN 
DBMS_OUTPUT.PUT_LINE('1'); --先打印第1行 
DBMS_OUTPUT.PUT(RPAD(1,9,' '));--先打印第2行的首个数字1，并用三个空格分隔数字 
DBMS_OUTPUT.PUT(RPAD(1,9,' '));--打印第2行的末尾数字1，并用三个空格分隔数字 
DBMS_OUTPUT.PUT_LINE(''); --打印换行符号 
FOR i IN 1 .. N LOOP --从第3行开始循环到第N行，计算每一行的数字并输出结果 
rowArray.EXTEND; --扩展数组长度，添加新元素，默认为0
END LOOP; 
rowArray(1):=1; --设置第2行首尾两个数为1 
rowArray(2):=1; 
FOR i IN 3 .. N LOOP --从第3行开始循环到第N行，计算每一行的数字并输出结果 
rowArray(i):=1; --设置每行首个数为1 
j:=i-1;
WHILE j>1 LOOP --计算第j行的数字，从右往左计算，直到第2个数字 
rowArray(j):=rowArray(j)+rowArray(j-1); 
j:=j-1; 
END LOOP; 
FOR j IN 1 .. i LOOP --打印第i行的数字 
DBMS_OUTPUT.PUT(RPAD(rowArray(j),9,' '));--打印数字并用三个空格分隔 
END LOOP; DBMS_OUTPUT.PUT_LINE(''); --打印换行符号 
END LOOP; END YHTriangle;

```

![](./save.png)

### 2.激活输出

```
SET SERVEROUTPUT ON SIZE UNLIMITED 
```

### 3.执行存储过程

```
EXECUTE hr.YHTriangle(5); 
```

### 4.实验结果

![](./YHTriangle.png)



## 实验结论

本次实验主要是将源代码转换为 ORACLE 存储过程，用于输出杨辉三角。在转换过程中，需要注意 ORACLE 的语法和函数的使用方法，最终完成了存储过程的编写，并成功输出了指定行数的杨辉三角并深入理解了杨辉三角的生成规律。掌握Oracle PL/SQL语言以及存储过程的编写。
