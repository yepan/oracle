# 实验3：创建分区表

姓名：叶攀

学号：202010414125

班级：1班



## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。



## 实验内容

本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。

两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。

新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。

orders表按订单日期（order_date）设置范围分区。

order_details表设置引用分区。

表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。

写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。

进行分区与不分区的对比实验。



## 实验步奏

1. 使用`sale`用户连接到`pdb`数据库。并且创建`orders`表

   ~~~sql
   CREATE TABLE orders 
   (
    order_id NUMBER(9, 0) NOT NULL
    , customer_name VARCHAR2(40 BYTE) NOT NULL 
    , customer_tel VARCHAR2(40 BYTE) NOT NULL 
    , order_date DATE NOT NULL 
    , employee_id NUMBER(6, 0) NOT NULL 
    , discount NUMBER(8, 2) DEFAULT 0 
    , trade_receivable NUMBER(8, 2) DEFAULT 0 
    , CONSTRAINT ORDERS_PK PRIMARY KEY 
     (
       ORDER_ID 
     )
   ) 
   TABLESPACE USERS 
   PCTFREE 10 INITRANS 1 
   STORAGE (   BUFFER_POOL DEFAULT ) 
   NOCOMPRESS NOPARALLEL 
   ~~~

   ![](./createorders.png)

2. 创建表`order_details`

   ~~~sql
   CREATE TABLE order_details
   (
   id NUMBER(9, 0) NOT NULL 
   , order_id NUMBER(10, 0) NOT NULL
   , product_id VARCHAR2(40 BYTE) NOT NULL 
   , product_num NUMBER(8, 2) NOT NULL 
   , product_price NUMBER(8, 2) NOT NULL 
   , CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
     (
       id 
     )
   , CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
   REFERENCES orders  (  order_id   )
   ENABLE
   ) 
   TABLESPACE USERS 
   PCTFREE 10 INITRANS 1 
   STORAGE ( BUFFER_POOL DEFAULT ) 
   NOCOMPRESS NOPARALLEL
   
   ~~~

   ![](./createdetails.png)

3. 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。

   ![](./B_Tree.png)

4. 新建两个序列，分别设置ordertable.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。

   ~~~sql
   CREATE SEQUENCE  SEQ_ORDERS  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
   CREATE SEQUENCE  SEQ_ORDER_DETAILS  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
   ~~~

   ![](./seq.png)

   将序列应用在两个表上

   ~~~sql
   ALTER TABLE orders MODIFY order_id DEFAULT SEQ_ORDERS.NEXTVAL;
   ALTER TABLE order_details MODIFY id DEFAULT SEQ_ORDER_DETAILS.NEXTVAL;
   ~~~

   ![](./syseq.png)

5. 设置分区

   ~~~sql
   CREATE TABLE ORDERS
   (
     ORDER_ID NUMBER(10, 0) NOT NULL
   , CUSTOMER_NAME VARCHAR2(40 BYTE) NOT NULL
   , CUSTOMER_TEL VARCHAR2(40 BYTE) NOT NULL
   , ORDER_DATE DATE NOT NULL
   , EMPLOYEE_ID NUMBER(6, 0) NOT NULL
   , DISCOUNT NUMBER(8, 2) DEFAULT 0
   , TRADE_RECEIVABLE NUMBER(8, 2) DEFAULT 0
   , CONSTRAINT ORDERS_PK PRIMARY KEY
     (
       ORDER_ID
     )
     USING INDEX
     (
         CREATE UNIQUE INDEX ORDERS_PK ON ORDERS (ORDER_ID ASC)
         LOGGING
         TABLESPACE USERS
         PCTFREE 10
         INITRANS 2
         STORAGE
         (
           BUFFER_POOL DEFAULT
         )
         NOPARALLEL
     )
     ENABLE
   )
   TABLESPACE USERS
   PCTFREE 10
   INITRANS 1
   STORAGE
   (
     BUFFER_POOL DEFAULT
   )
   NOCOMPRESS
   NOPARALLEL
   PARTITION BY RANGE (ORDER_DATE)
   (
     PARTITION PARTITION_2015 VALUES LESS THAN (TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
     NOLOGGING
     TABLESPACE USERS
     PCTFREE 10
     INITRANS 1
     STORAGE
     (
       INITIAL 8388608
       NEXT 1048576
       MINEXTENTS 1
       MAXEXTENTS UNLIMITED
       BUFFER_POOL DEFAULT
     )
     NOCOMPRESS NO INMEMORY
   , PARTITION PARTITION_2016 VALUES LESS THAN (TO_DATE(' 2017-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
     NOLOGGING
     TABLESPACE USERS
     PCTFREE 10
     INITRANS 1
     STORAGE
     (
       BUFFER_POOL DEFAULT
     )
     NOCOMPRESS NO INMEMORY
   , PARTITION PARTITION_2017 VALUES LESS THAN (TO_DATE(' 2018-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
     NOLOGGING
     TABLESPACE USERS
     PCTFREE 10
     INITRANS 1
     STORAGE
     (
       BUFFER_POOL DEFAULT
     )
     NOCOMPRESS NO INMEMORY
   , PARTITION PARTITION_2018 VALUES LESS THAN (TO_DATE(' 2019-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
     NOLOGGING
     TABLESPACE USERS02
     PCTFREE 10
     INITRANS 1
     STORAGE
     (
       BUFFER_POOL DEFAULT
     )
     NOCOMPRESS NO INMEMORY
   , PARTITION PARTITION_2019 VALUES LESS THAN (TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
     NOLOGGING
     TABLESPACE USERS02
     PCTFREE 10
     INITRANS 1
     STORAGE
     (
       BUFFER_POOL DEFAULT
     )
     NOCOMPRESS NO INMEMORY
   , PARTITION PARTITION_2020 VALUES LESS THAN (TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
     NOLOGGING
     TABLESPACE USERS02
     PCTFREE 10
     INITRANS 1
     STORAGE
     (
       BUFFER_POOL DEFAULT
     )
     NOCOMPRESS NO INMEMORY
   , PARTITION PARTITION_2021 VALUES LESS THAN (TO_DATE(' 2022-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
     NOLOGGING
     TABLESPACE USERS03
     PCTFREE 10
     INITRANS 1
     STORAGE
     (
       BUFFER_POOL DEFAULT
     )
     NOCOMPRESS NO INMEMORY
   );
   
   
   CREATE TABLE order_details
   (
   id NUMBER(10, 0) NOT NULL
   , order_id NUMBER(10, 0) NOT NULL
   , product_name VARCHAR2(40 BYTE) NOT NULL
   , product_num NUMBER(8, 2) NOT NULL
   , product_price NUMBER(8, 2) NOT NULL
   , CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
   REFERENCES orders  (  order_id   )
   ENABLE
   )
   TABLESPACE USERS
   PCTFREE 10 INITRANS 1
   STORAGE (BUFFER_POOL DEFAULT )
   NOCOMPRESS NOPARALLEL
   PARTITION BY REFERENCE (order_details_fk1);
   ~~~

   ![](./fq.png)

6. 写入数据

   ~~~sql
   declare
       i integer;
       y integer;
       m integer;
       d integer;
       str VARCHAR2(100);
       order_id integer;
   
   BEGIN
       i:=1;
       while i < 100010 loop
       
       y := trunc(dbms_random.value(2015,2021));
       m := trunc(dbms_random.value(1,12));
       d := trunc(dbms_random.value(1,28));
       
       str := y||'-'||m||'-'||d;
       
       INSERT INTO orders (order_id, customer_name, customer_tel, order_date, employee_id)
       VALUES (i, 'customer'||i, 'tel'||i, to_date(str,'yyyy-MM-dd'), mod(1,10));
   
       order_id := i;
   
       INSERT INTO order_details (order_id, product_name, product_num, product_price)
       VALUES (order_id, 'product1', 1, 10);
       INSERT INTO order_details (order_id, product_name, product_num, product_price)
       VALUES (order_id, 'product2', 2, 20);
       INSERT INTO order_details (order_id, product_name, product_num, product_price)
       VALUES (order_id, 'product3', 3, 30);
       INSERT INTO order_details (order_id, product_name, product_num, product_price)
       VALUES (order_id, 'product4', 4, 40);
       INSERT INTO order_details (order_id, product_name, product_num, product_price)
       VALUES (order_id, 'product5', 5, 50);
       
       i:=i+1;
       
       end loop;
       
       commit;
   END;
   ~~~

   ![](./data1.png)

   ![](./data2.png)

7. 进行分区与不分区对比

   建立无分区的表

   ~~~sql
   
   CREATE TABLE ORDERS_NOSPACE 
   (
     ORDER_ID NUMBER(10, 0) NOT NULL 
   , CUSTOMER_NAME VARCHAR2(40 BYTE) NOT NULL 
   , CUSTOMER_TEL VARCHAR2(40 BYTE) NOT NULL 
   , ORDER_DATE DATE NOT NULL 
   , EMPLOYEE_ID NUMBER(6, 0) DEFAULT 0 
   , DISCOUNT NUMBER(8, 2) DEFAULT 0 
   , CONSTRAINT ORDERS_ID_ORDERS_DETAILS PRIMARY KEY 
     (
       ORDER_ID 
     )
     USING INDEX 
     (
         CREATE UNIQUE INDEX ORDERS_ID_ORDERS_DETAILS ON ORDERS_NOSPACE (ORDER_ID ASC) 
         LOGGING 
         TABLESPACE USERS 
         PCTFREE 10 
         INITRANS 2 
         STORAGE 
         ( 
           BUFFER_POOL DEFAULT 
         ) 
         NOPARALLEL 
     )
     ENABLE 
   ) 
   LOGGING 
   TABLESPACE USERS 
   PCTFREE 10 
   INITRANS 1 
   STORAGE 
   ( 
     BUFFER_POOL DEFAULT 
   ) 
   NOCOMPRESS 
   NO INMEMORY 
   NOPARALLEL;
   
   CREATE TABLE ORDER_DETAILS_NOSPACE 
   (
     ID NUMBER(10, 0) NOT NULL 
   , ORDER_ID NUMBER(10, 0) NOT NULL 
   , PRODUCT_NAME VARCHAR2(40 BYTE) NOT NULL 
   , PRODUCT_NUM NUMBER(8, 2) NOT NULL 
   , PRODUCT_PRICE NUMBER(8, 2) NOT NULL 
   ) 
   LOGGING 
   TABLESPACE USERS 
   PCTFREE 10 
   INITRANS 1 
   STORAGE 
   ( 
     INITIAL 65536 
     NEXT 1048576 
     MINEXTENTS 1 
     MAXEXTENTS UNLIMITED 
     BUFFER_POOL DEFAULT 
   ) 
   NOCOMPRESS 
   NO INMEMORY 
   NOPARALLEL;
   
   ALTER TABLE ORDER_DETAILS_NOSPACE
   ADD CONSTRAINT ORDERS_FOREIGN_ORDERS_DETAILS FOREIGN KEY
   (
     ORDER_ID 
   )
   REFERENCES ORDERS_NOSPACE
   (
     ORDER_ID 
   )
   ENABLE;
   
   
   ~~~

   ![](./nospace.png)

   **数据复制**

   ~~~sql
    INSERT INTO order_details_nospace (id, order_id, product_name, product_num, product_price) 
    SELECT id, order_id, product_name, product_num, product_price FROM order_details;
   ~~~

   使用类似上面的语句将数据复制到新建立的无分区表中。

   ![](./copy1.png)

   ![](./copy2.png)

   执行查询语句，对比双方的查询效率。

   **有分区**

   ~~~sql
   select * from orders where order_date between to_date('2017-1-1','yyyy-mm-dd') and to_date('2021-6-1','yyyy-mm-dd');
   ~~~

   ![](./chaxun1.jpg)

   **无分区**

   ~~~sql
   select * from orders_nospace where order_date between to_date('2017-1-1','yyyy-mm-dd') and to_date('2021-6-1','yyyy-mm-dd');
   ~~~

   ![](./chaxun2.jpg)



## 实验结果

仅从图上看的话，分区表并不能得到太大的优势，一方面是查询语句的复杂度不够，另一方面是数据量太小。数据量在较小的情况下可以说分区表甚至没有什么优势，且增加了维护成本。当使用主键或唯一索引进行查询时，分区表和不分区表的查询性能差别不大。但是当使用非唯一索引进行查询时，由于sql需要扫描整个索引来定位符合条件的记录，在大型数据库中这种操作会变得很慢。而使用了分区后，在执行非唯一索引查询时可以只扫描特定的一个或几个子分区，从而提高了查询速度。

- 分区的优点：
  - 分区可以提高查询性能，尤其是在处理大量数据时，查询只需要访问特定分区，而不是整个表。
  - 分区可以简化数据维护和管理，例如备份、恢复和数据删除等操作可以针对单个分区进行。
  - 分区可以提供更好的数据组织结构，根据业务需求进行合理的分区设计可以提高查询效率和数据访问的灵活性。
- 分区的缺点：
  - 分区引入了更复杂的表结构和管理维护成本。
  - 分区可能导致更多的存储空间占用，因为每个分区都需要额外的存储空间来存储分区的元数据和索引。
