CREATE TABLESPACE data_tablespace
DATAFILE '/home/oracle/app/oracle/oradata/orcl/data_tablespace.dbf' SIZE 500M 
AUTOEXTEND ON NEXT 100M MAXSIZE 5G; 

CREATE TABLESPACE index_tablespace
DATAFILE '/home/oracle/app/oracle/oradata/orcl/index_tablespace.dbf' SIZE 500M  
AUTOEXTEND ON NEXT 50M MAXSIZE 3G; 

CREATE TABLE Products (
  ProductID NUMBER PRIMARY KEY,
  ProductName VARCHAR2(100),
  ProductDescription VARCHAR2(255),
  Price NUMBER(10, 2),
  Stock NUMBER
);

CREATE TABLE Customers (
  CustomerID NUMBER PRIMARY KEY,
  Name VARCHAR2(100),
  Contact VARCHAR2(100)
);

CREATE TABLE Orders (
  OrderID NUMBER PRIMARY KEY,
  CustomerID NUMBER,
  OrderDate DATE,
  TotalAmount NUMBER(10, 2),
  CONSTRAINT fk_customer FOREIGN KEY (CustomerID) REFERENCES Customers(CustomerID)
);

CREATE TABLE Inventory (
  ProductID NUMBER,
  Quantity NUMBER,
  CONSTRAINT fk_product FOREIGN KEY (ProductID) REFERENCES Products(ProductID)
);

CREATE TABLE OrderItems (
  OrderItemID NUMBER PRIMARY KEY,
  OrderID NUMBER,
  ProductID NUMBER,
  Quantity NUMBER,
  UnitPrice NUMBER(10, 2),
  TotalAmount NUMBER(10, 2),
  CONSTRAINT fk_orderitem_order FOREIGN KEY (OrderID) REFERENCES Orders(OrderID),
  CONSTRAINT fk_orderitem_product FOREIGN KEY (ProductID) REFERENCES Products(ProductID)
);



CREATE USER admin IDENTIFIED BY root;
GRANT CREATE SESSION TO admin;
GRANT DBA TO admin;

CREATE USER yy IDENTIFIED BY yy;
GRANT CREATE SESSION TO yy;
GRANT SELECT, INSERT, UPDATE, DELETE ON Products TO yy;
GRANT SELECT, INSERT, UPDATE, DELETE ON Orders TO yy;
GRANT SELECT, INSERT, UPDATE, DELETE ON Customers TO yy;

CREATE OR REPLACE PACKAGE SalesPackage AS

  PROCEDURE CreateOrder(
    p_CustomerID IN NUMBER,
    p_ProductID IN NUMBER,
    p_Quantity IN NUMBER
  );

  PROCEDURE EditOrder(
    p_OrderID IN NUMBER,
    p_ProductID IN NUMBER,
    p_Quantity IN NUMBER
  );

  PROCEDURE QueryOrder(
    p_OrderID IN NUMBER
  );

  FUNCTION CalculateOrderTotal(
    p_OrderID IN NUMBER
  ) RETURN NUMBER;

  FUNCTION GetProductStock(
    p_ProductID IN NUMBER
  ) RETURN NUMBER;

END SalesPackage;
/

CREATE OR REPLACE PACKAGE BODY SalesPackage AS

  PROCEDURE CreateOrder(
    p_CustomerID IN NUMBER,
    p_ProductID IN NUMBER,
    p_Quantity IN NUMBER
  ) IS
    v_order_id NUMBER;
  BEGIN
    -- 创建订单
    INSERT INTO Orders (CustomerID, OrderDate)
    VALUES (p_CustomerID, SYSDATE)
    RETURNING OrderID INTO v_order_id;
    
    -- 创建订单项
    INSERT INTO OrderItems (OrderID, ProductID, Quantity, UnitPrice, TotalAmount)
    VALUES (v_order_id, p_ProductID, p_Quantity, (SELECT Price FROM Products WHERE ProductID = p_ProductID), p_Quantity * (SELECT Price FROM Products WHERE ProductID = p_ProductID));

    COMMIT;
  END CreateOrder;

  PROCEDURE EditOrder(
    p_OrderID IN NUMBER,
    p_ProductID IN NUMBER,
    p_Quantity IN NUMBER
  ) IS
  BEGIN
    -- 编辑订单项
    UPDATE OrderItems
    SET ProductID = p_ProductID,
        Quantity = p_Quantity,
        UnitPrice = (SELECT Price FROM Products WHERE ProductID = p_ProductID),
        TotalAmount = p_Quantity * (SELECT Price FROM Products WHERE ProductID = p_ProductID)
    WHERE OrderItemID = p_OrderID;

    COMMIT;
  END EditOrder;

  PROCEDURE QueryOrder(
    p_OrderID IN NUMBER
  ) IS
    v_order_date DATE;
    v_customer_name VARCHAR2(100);
    v_product_name VARCHAR2(100);
    v_quantity NUMBER;
    v_total_amount NUMBER;
  BEGIN
    -- 查询订单信息
    SELECT o.OrderDate, c.Name, p.ProductName, oi.Quantity, oi.TotalAmount
    INTO v_order_date, v_customer_name, v_product_name, v_quantity, v_total_amount
    FROM Orders o
    INNER JOIN Customers c ON o.CustomerID = c.CustomerID
    INNER JOIN OrderItems oi ON o.OrderID = oi.OrderID
    INNER JOIN Products p ON oi.ProductID = p.ProductID
    WHERE o.OrderID = p_OrderID;

    -- 输出订单信息
    DBMS_OUTPUT.PUT_LINE('订单ID: ' || p_OrderID);
    DBMS_OUTPUT.PUT_LINE('订单日期: ' || v_order_date);
    DBMS_OUTPUT.PUT_LINE('客户姓名: ' || v_customer_name);
    DBMS_OUTPUT.PUT_LINE('商品名称: ' || v_product_name);
    DBMS_OUTPUT.PUT_LINE('数量: ' || v_quantity);
    DBMS_OUTPUT.PUT_LINE('总金额: ' || v_total_amount);
  END QueryOrder;

  FUNCTION CalculateOrderTotal(
    p_OrderID IN NUMBER
  ) RETURN NUMBER IS
    v_total_amount NUMBER;
  BEGIN
    -- 计算订单总金额
    SELECT SUM(TotalAmount)
    INTO v_total_amount
    FROM OrderItems
    WHERE OrderID = p_OrderID;

    RETURN v_total_amount;
  END CalculateOrderTotal;

  FUNCTION GetProductStock(
    p_ProductID IN NUMBER
  ) RETURN NUMBER IS
    v_stock NUMBER;
  BEGIN
    -- 查询商品库存
    SELECT Stock
    INTO v_stock
    FROM Products
    WHERE ProductID = p_ProductID;

    RETURN v_stock;
  END GetProductStock;

END SalesPackage;
/


GRANT EXECUTE ON SalesPackage TO yy;

DECLARE
  v_product_id NUMBER;
  v_product_name VARCHAR2(100);
  v_product_description VARCHAR2(255);
  v_price NUMBER(10, 2);
  v_stock NUMBER;
BEGIN
  FOR i IN 1..20000 LOOP

    v_product_id := i;
    v_product_name := 'Product ' || i;
    v_product_description := 'Description ' || i;
    v_price := ROUND(DBMS_RANDOM.VALUE(10, 1000), 2);
    v_stock := ROUND(DBMS_RANDOM.VALUE(0, 100), 0);
    

    INSERT INTO Products (ProductID, ProductName, ProductDescription, Price, Stock)
    VALUES (v_product_id, v_product_name, v_product_description, v_price, v_stock);
    

    COMMIT;
  END LOOP;
END;
/

DECLARE
  v_customer_id NUMBER;
  v_name VARCHAR2(100);
  v_contact VARCHAR2(100);
BEGIN
  FOR i IN 1..10000 LOOP

    v_customer_id := i;
    v_name := 'Customer ' || i;
    v_contact := 'Contact ' || i;
    

    INSERT INTO Customers (CustomerID, Name, Contact)
    VALUES (v_customer_id, v_name, v_contact);
    

    COMMIT;
  END LOOP;
END;
/
DECLARE
  v_order_id NUMBER;
  v_customer_id NUMBER;
  v_order_date DATE;
  v_total_amount NUMBER(10, 2);
BEGIN
  FOR i IN 1..40000 LOOP

    v_order_id := i;
    v_customer_id := ROUND(DBMS_RANDOM.VALUE(1, 10000), 0); -- 假设有10000个客户
    v_order_date := SYSDATE - ROUND(DBMS_RANDOM.VALUE(1, 365), 0); -- 假设订单日期在最近一年内
    v_total_amount := ROUND(DBMS_RANDOM.VALUE(100, 1000), 2);
    

    INSERT INTO Orders (OrderID, CustomerID, OrderDate, TotalAmount)
    VALUES (v_order_id, v_customer_id, v_order_date, v_total_amount);
    

    COMMIT;
  END LOOP;
END;
/

DECLARE
  v_product_id NUMBER;
  v_quantity NUMBER;
BEGIN
  FOR i IN 1..20000 LOOP

    v_product_id := ROUND(DBMS_RANDOM.VALUE(1, 20000), 0); 
    v_quantity := ROUND(DBMS_RANDOM.VALUE(0, 100), 0);
    

    INSERT INTO Inventory (ProductID, Quantity)
    VALUES (v_product_id, v_quantity);
    

    COMMIT;
  END LOOP;
END;
/

DECLARE
  v_order_id NUMBER;
  v_product_id NUMBER;
  v_quantity NUMBER;
  v_unit_price NUMBER(10, 2);
  v_total_amount NUMBER(10, 2);
BEGIN
  FOR i IN 1..10000 LOOP

    v_order_id := i;
    v_product_id := ROUND(DBMS_RANDOM.VALUE(1, 20000), 0); 
    v_quantity := ROUND(DBMS_RANDOM.VALUE(1, 10), 0);
    v_unit_price := ROUND(DBMS_RANDOM.VALUE(10, 100), 2);
    v_total_amount := v_quantity * v_unit_price;
    

    INSERT INTO OrderItems (OrderItemID, OrderID, ProductID, Quantity, UnitPrice, TotalAmount)
    VALUES (i, v_order_id, v_product_id, v_quantity, v_unit_price, v_total_amount);
    

    COMMIT;
  END LOOP;
END;
/



