# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。



### 表空间的创建

- ，一个用于存储数据，另一个用于存储索引。你可以将它们命名为"data_tablespace"和"index_tablespace"，并为它们分配了适当的大小和自动扩展策略。

  - 创建data_tablespace代码：

    ```sql
    CREATE TABLESPACE data_tablespace
    DATAFILE '/home/oracle/app/oracle/oradata/orcl/data_tablespace.dbf' SIZE 500M 
    AUTOEXTEND ON NEXT 100M MAXSIZE 5G; 
    ```

    1.设置初始大小为：500M

    2.自动扩展策略，每次扩展100M，最大不超过5G

  - 创建index_tablespace代码：

    ```sql
    CREATE TABLESPACE index_tablespace
    DATAFILE '/home/oracle/app/oracle/oradata/orcl/index_tablespace.dbf' SIZE 500M  
    AUTOEXTEND ON NEXT 50M MAXSIZE 3G; 
    ```

    1.设置初始大小为：500M

    2.自动扩展策略，每次扩展50M，最大不超过3G

  这样设计两个表空间，一个用于存储数据，另一个用于存储索引，有以下目的和优势：

  1. 提升性能：将数据和索引存储在不同的表空间中可以提高系统的读写性能。索引表空间通常需要更快的访问速度，而数据表空间可能需要更大的存储空间。通过将它们分离，可以根据需求对两个表空间进行独立的优化和调整，以获得更好的性能表现。
  2. 管理和维护的灵活性：独立的表空间允许对数据和索引进行分开管理和维护。例如，可以单独备份和还原索引表空间，执行索引重建或重新组织操作，而不会影响数据表空间。这样可以简化管理任务，并使维护操作更加高效。
  3. 空间管理：将数据和索引存储在不同的表空

### 表设计方案

#### 创建商品表（Products）

- 创建代码块：

  ```sql
  CREATE TABLE Products (
    ProductID NUMBER PRIMARY KEY,
    ProductName VARCHAR2(100),
    ProductDescription VARCHAR2(255),
    Price NUMBER(10, 2),
    Stock NUMBER
  );
  ```

- 相关阐述：
  - ProductID：作为主键，用于唯一标识每个商品的数字类型字段。
  - ProductName：存储商品名称的字符串类型字段，最大长度为100。
  - ProductDescription：存储商品描述的字符串类型字段，最大长度为255。
  - Price：存储商品价格的数值类型字段，采用10位数和2位小数的精度。
  - Stock：存储商品库存数量的数值类型字段。

#### 创建客户表（Customers）

- 代码块：

  ```sql
  CREATE TABLE Customers (
    CustomerID NUMBER PRIMARY KEY,
    Name VARCHAR2(100),
    Contact VARCHAR2(100)
  );
  ```

- 相关阐述：

  - CustomerID：作为主键，用于唯一标识每个客户的数字类型字段。
  - Name：存储客户姓名的字符串类型字段，最大长度为100。
  - Contact：记录客户联系方式的字符串类型字段，最大长度为100。

#### 创建订单表（Orders）

- 代码块：

  ```sql
  CREATE TABLE Orders (
    OrderID NUMBER PRIMARY KEY,
    CustomerID NUMBER,
    OrderDate DATE,
    TotalAmount NUMBER(10, 2),
    CONSTRAINT fk_customer FOREIGN KEY (CustomerID) REFERENCES Customers(CustomerID)
  );
  ```

- 相关阐述：
  - OrderID：作为主键，用于唯一标识每个订单的数字类型字段。
  - CustomerID：作为外键，关联到客户表中的客户ID，用于表示订单所属的客户。
  - OrderDate：记录订单的下单时间的日期类型字段。
  - TotalAmount：存储订单的总金额的数值类型字段，采用10位数和2位小数的精度。

#### 创建库存表（Inventory）

- 代码块：

  ```sql
  CREATE TABLE Inventory (
    ProductID NUMBER,
    Quantity NUMBER,
    CONSTRAINT fk_product FOREIGN KEY (ProductID) REFERENCES Products(ProductID)
  );
  ```

- 相关阐述：

  - ProductID：作为外键，关联到商品表中的商品ID，用于表示库存所属的商品。
  - Quantity：记录商品的库存数量的数值类型字段。

  在上述示例中，还使用了外键约束（FOREIGN KEY）来确保ProductID字段的值与Products表中的ProductID字段值匹配。

#### 创建订单项表（OrderItems）

- 代码块：

  ```sql
  CREATE TABLE OrderItems (
    OrderItemID NUMBER PRIMARY KEY,
    OrderID NUMBER,
    ProductID NUMBER,
    Quantity NUMBER,
    UnitPrice NUMBER(10, 2),
    TotalAmount NUMBER(10, 2),
    CONSTRAINT fk_orderitem_order FOREIGN KEY (OrderID) REFERENCES Orders(OrderID),
    CONSTRAINT fk_orderitem_product FOREIGN KEY (ProductID) REFERENCES Products(ProductID)
  );
  ```

- 相关阐述：

  - `CREATE TABLE OrderItems`：创建名为"OrderItems"的表。
  - `OrderItemID NUMBER PRIMARY KEY`：定义了一个名为"OrderItemID"的字段，它是主键字段，用于唯一标识每个订单项。
  - `OrderID NUMBER`：定义了一个名为"OrderID"的字段，用于存储关联订单的订单ID。
  - `ProductID NUMBER`：定义了一个名为"ProductID"的字段，用于存储关联商品的商品ID。
  - `Quantity NUMBER`：定义了一个名为"Quantity"的字段，用于记录每个订单项中商品的数量。
  - `UnitPrice NUMBER(10, 2)`：定义了一个名为"UnitPrice"的字段，用于存储每个订单项中商品的单价。这里使用了数值类型，并指定了总位数为10，小数位数为2。
  - `TotalAmount NUMBER(10, 2)`：定义了一个名为"TotalAmount"的字段，用于存储订单项的总金额。同样使用了数值类型，并指定了总位数为10，小数位数为2。
  - `CONSTRAINT fk_orderitem_order FOREIGN KEY (OrderID) REFERENCES Orders(OrderID)`：这是一个外键约束，将"OrderID"字段与"Orders"表中的"OrderID"字段进行关联。这样可以确保"OrderID"的值在"Orders"表中存在。
  - `CONSTRAINT fk_orderitem_product FOREIGN KEY (ProductID) REFERENCES Products(ProductID)`：这也是一个外键约束，将"ProductID"字段与"Products"表中的"ProductID"字段进行关联。这样可以确保"ProductID"的值在"Products"表中存在。

### 权限及用户分配

#### 创建管理员

- 代码块：

  ```sql
  CREATE USER admin IDENTIFIED BY root;
  GRANT CREATE SESSION TO admin;
  GRANT DBA TO admin;
  ```

  创建一个admin管理员用户，密码为：root.并为该用户分配了创建会话的权限和DBA（数据库管理员）角色，这将赋予管理员用户对所有表的完全访问权限以及管理用户和执行所有存储过程的权限。

### 普通用户的创建

- 代码块

  ```sql
  CREATE USER yy IDENTIFIED BY yy;
  GRANT CREATE SESSION TO yy;
  GRANT SELECT, INSERT, UPDATE, DELETE ON Products TO yy;
  GRANT SELECT, INSERT, UPDATE, DELETE ON Orders TO yy;
  GRANT SELECT, INSERT, UPDATE, DELETE ON Customers TO yy;
  ```

  以上代码创建了一个名为"yy"的普通用户，并为该用户分配了创建会话的权限以及对商品表（Products）、订单表（Orders）和客户表（Customers）的读取和写入权限。这样普通用户就可以执行相关的操作。

- 授予普通用户执行存储过程的权限：

  ```
  GRANT EXECUTE ON SalesPackage TO yy;
  ```

### 程序包设计

#### 创建程序包规格（Specification）：

- 代码块：

  ``` 
  CREATE OR REPLACE PACKAGE SalesPackage AS
  
    PROCEDURE CreateOrder(
      p_CustomerID IN NUMBER,
      p_ProductID IN NUMBER,
      p_Quantity IN NUMBER
    );
  
    PROCEDURE EditOrder(
      p_OrderID IN NUMBER,
      p_ProductID IN NUMBER,
      p_Quantity IN NUMBER
    );
  
    PROCEDURE QueryOrder(
      p_OrderID IN NUMBER
    );
  
    FUNCTION CalculateOrderTotal(
      p_OrderID IN NUMBER
    ) RETURN NUMBER;
  
    FUNCTION GetProductStock(
      p_ProductID IN NUMBER
    ) RETURN NUMBER;
  
  END SalesPackage;
  /
  ```

- 阐述：上述代码创建了一个名为"SalesPackage"的程序包规格，其中包含了存储过程和函数的声明。具体包括：

  - `CreateOrder` 存储过程用于创建订单，接受客户ID、商品ID和数量作为输入参数。
  - `EditOrder` 存储过程用于编辑订单，接受订单ID、商品ID和数量作为输入参数。
  - `QueryOrder` 存储过程用于查询订单，接受订单ID作为输入参数。
  - `CalculateOrderTotal` 函数用于计算订单总金额，接受订单ID作为输入参数，并返回总金额。
  - `GetProductStock` 函数用于查询商品库存，接受商品ID作为输入参数，并返回库存数量。

#### 创建程序包体（Body）

- 代码块：

  ```
  CREATE OR REPLACE PACKAGE BODY SalesPackage AS
  
    PROCEDURE CreateOrder(
      p_CustomerID IN NUMBER,
      p_ProductID IN NUMBER,
      p_Quantity IN NUMBER
    ) IS
      v_order_id NUMBER;
    BEGIN
      -- 创建订单
      INSERT INTO Orders (CustomerID, OrderDate)
      VALUES (p_CustomerID, SYSDATE)
      RETURNING OrderID INTO v_order_id;
      
      -- 创建订单项
      INSERT INTO OrderItems (OrderID, ProductID, Quantity, UnitPrice, TotalAmount)
      VALUES (v_order_id, p_ProductID, p_Quantity, (SELECT Price FROM Products WHERE ProductID = p_ProductID), p_Quantity * (SELECT Price FROM Products WHERE ProductID = p_ProductID));
  
      COMMIT;
    END CreateOrder;
  
    PROCEDURE EditOrder(
      p_OrderID IN NUMBER,
      p_ProductID IN NUMBER,
      p_Quantity IN NUMBER
    ) IS
    BEGIN
      -- 编辑订单项
      UPDATE OrderItems
      SET ProductID = p_ProductID,
          Quantity = p_Quantity,
          UnitPrice = (SELECT Price FROM Products WHERE ProductID = p_ProductID),
          TotalAmount = p_Quantity * (SELECT Price FROM Products WHERE ProductID = p_ProductID)
      WHERE OrderItemID = p_OrderID;
  
      COMMIT;
    END EditOrder;
  
    PROCEDURE QueryOrder(
      p_OrderID IN NUMBER
    ) IS
      v_order_date DATE;
      v_customer_name VARCHAR2(100);
      v_product_name VARCHAR2(100);
      v_quantity NUMBER;
      v_total_amount NUMBER;
    BEGIN
      -- 查询订单信息
      SELECT o.OrderDate, c.Name, p.ProductName, oi.Quantity, oi.TotalAmount
      INTO v_order_date, v_customer_name, v_product_name, v_quantity, v_total_amount
      FROM Orders o
      INNER JOIN Customers c ON o.CustomerID = c.CustomerID
      INNER JOIN OrderItems oi ON o.OrderID = oi.OrderID
      INNER JOIN Products p ON oi.ProductID = p.ProductID
      WHERE o.OrderID = p_OrderID;
  
      -- 输出订单信息
      DBMS_OUTPUT.PUT_LINE('订单ID: ' || p_OrderID);
      DBMS_OUTPUT.PUT_LINE('订单日期: ' || v_order_date);
      DBMS_OUTPUT.PUT_LINE('客户姓名: ' || v_customer_name);
      DBMS_OUTPUT.PUT_LINE('商品名称: ' || v_product_name);
      DBMS_OUTPUT.PUT_LINE('数量: ' || v_quantity);
      DBMS_OUTPUT.PUT_LINE('总金额: ' || v_total_amount);
    END QueryOrder;
  
    FUNCTION CalculateOrderTotal(
      p_OrderID IN NUMBER
    ) RETURN NUMBER IS
      v_total_amount NUMBER;
    BEGIN
      -- 计算订单总金额
      SELECT SUM(TotalAmount)
      INTO v_total_amount
      FROM OrderItems
      WHERE OrderID = p_OrderID;
  
      RETURN v_total_amount;
    END CalculateOrderTotal;
  
    FUNCTION GetProductStock(
      p_ProductID IN NUMBER
    ) RETURN NUMBER IS
      v_stock NUMBER;
    BEGIN
      -- 查询商品库存
      SELECT Stock
      INTO v_stock
      FROM Products
      WHERE ProductID = p_ProductID;
  
      RETURN v_stock;
    END GetProductStock;
  
  END SalesPackage;
  /
  
  ```

### 向表中插入数据

#### 向商品表中插入数据

- 代码块：

  ```
  DECLARE
    v_product_id NUMBER;
    v_product_name VARCHAR2(100);
    v_product_description VARCHAR2(255);
    v_price NUMBER(10, 2);
    v_stock NUMBER;
  BEGIN
    FOR i IN 1..20000 LOOP
      -- 生成随机数据
      v_product_id := i;
      v_product_name := 'Product ' || i;
      v_product_description := 'Description ' || i;
      v_price := ROUND(DBMS_RANDOM.VALUE(10, 1000), 2);
      v_stock := ROUND(DBMS_RANDOM.VALUE(0, 100), 0);
      
      -- 插入数据
      INSERT INTO Products (ProductID, ProductName, ProductDescription, Price, Stock)
      VALUES (v_product_id, v_product_name, v_product_description, v_price, v_stock);
      
      -- 提交事务
      COMMIT;
    END LOOP;
  END;
  /
  ```

  以上代码使用了一个循环，在每次迭代中生成随机的商品数据，并将其插入到商品表（Products）中。请注意，代码中使用了`DBMS_RANDOM.VALUE`函数来生成随机数。

#### 向客户表插入数据

- 代码块：

  ```
  DECLARE
    v_customer_id NUMBER;
    v_name VARCHAR2(100);
    v_contact VARCHAR2(100);
  BEGIN
    FOR i IN 1..10000 LOOP
      -- 生成随机数据
      v_customer_id := i;
      v_name := 'Customer ' || i;
      v_contact := 'Contact ' || i;
      
      -- 插入数据
      INSERT INTO Customers (CustomerID, Name, Contact)
      VALUES (v_customer_id, v_name, v_contact);
      
      -- 提交事务
      COMMIT;
    END LOOP;
  END;
  /
  
  ```

#### 向订单表插入数据

- 代码块：

  ```
  DECLARE
    v_order_id NUMBER;
    v_customer_id NUMBER;
    v_order_date DATE;
    v_total_amount NUMBER(10, 2);
  BEGIN
    FOR i IN 1..40000 LOOP
      -- 生成随机数据
      v_order_id := i;
      v_customer_id := ROUND(DBMS_RANDOM.VALUE(1, 10000), 0); -- 假设有10000个客户
      v_order_date := SYSDATE - ROUND(DBMS_RANDOM.VALUE(1, 365), 0); -- 假设订单日期在最近一年内
      v_total_amount := ROUND(DBMS_RANDOM.VALUE(100, 1000), 2);
      
      -- 插入数据
      INSERT INTO Orders (OrderID, CustomerID, OrderDate, TotalAmount)
      VALUES (v_order_id, v_customer_id, v_order_date, v_total_amount);
      
      -- 提交事务
      COMMIT;
    END LOOP;
  END;
  /
  
  ```

#### 向库存表插入数据

- 代码块

  ```
  DECLARE
    v_product_id NUMBER;
    v_quantity NUMBER;
  BEGIN
    FOR i IN 1..20000 LOOP
      -- 生成随机数据
      v_product_id := ROUND(DBMS_RANDOM.VALUE(1, 20000), 0); 
      v_quantity := ROUND(DBMS_RANDOM.VALUE(0, 100), 0);
      
      -- 插入数据
      INSERT INTO Inventory (ProductID, Quantity)
      VALUES (v_product_id, v_quantity);
      
      -- 提交事务
      COMMIT;
    END LOOP;
  END;
  /
  ```

#### 向订单项表中插入数据

- 代码块：

  ```
  DECLARE
    v_order_id NUMBER;
    v_product_id NUMBER;
    v_quantity NUMBER;
    v_unit_price NUMBER(10, 2);
    v_total_amount NUMBER(10, 2);
  BEGIN
    FOR i IN 1..10000 LOOP
      -- 生成随机数据
      v_order_id := i; -- 假设订单ID从1递增
      v_product_id := ROUND(DBMS_RANDOM.VALUE(1, 20000), 0); -- 假设有20000个商品
      v_quantity := ROUND(DBMS_RANDOM.VALUE(1, 10), 0);
      v_unit_price := ROUND(DBMS_RANDOM.VALUE(10, 100), 2);
      v_total_amount := v_quantity * v_unit_price;
      
      -- 插入数据
      INSERT INTO OrderItems (OrderItemID, OrderID, ProductID, Quantity, UnitPrice, TotalAmount)
      VALUES (i, v_order_id, v_product_id, v_quantity, v_unit_price, v_total_amount);
      
      -- 提交事务
      COMMIT;
    END LOOP;
  END;
  /
  
  ```

### 备份方案

1. 设计定期的备份策略：确定完全备份和增量备份的时间间隔和频率。通常建议进行定期的完全备份（如每周一次），并在此基础上执行增量备份（如每日一次）。
2. 使用Oracle的备份工具（如RMAN）进行数据库备份：使用RMAN命令行工具或RMAN脚本来执行数据库备份操作。RMAN提供了许多备份选项和命令，如备份数据库、备份表空间、备份数据文件等。
3. 将备份数据存储在独立的存储介质上：选择合适的存储介质来存储备份数据，例如磁盘、磁带库或云存储。确保备份数据与数据库主机分离，以提高数据的安全性和可靠性。
4. 定期测试和恢复备份：定期测试备份的可用性和完整性，以确保备份数据的有效性。可以创建一个测试环境，在该环境中恢复备份数据，并验证数据库的完整性和可用性。这有助于确保在实际灾难发生时能够成功恢复数据。

- 执行完全备份：

  ```
  sqlCopy code
  RMAN> BACKUP DATABASE PLUS ARCHIVELOG;
  ```

- 执行增量备份：

  ```
  sqlCopy code
  RMAN> BACKUP INCREMENTAL LEVEL 1 DATABASE PLUS ARCHIVELOG;
  ```

- 恢复数据库：

  ```
  sqlCopy codeRMAN> RUN {
        ALLOCATE CHANNEL ch1 TYPE DISK;
        SET UNTIL TIME 'yyyy-mm-dd hh24:mi:ss';
        RESTORE DATABASE;
        RECOVER DATABASE;
      }
  ```
