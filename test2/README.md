# 实验2：用户权限管理

- 学号：202010414125
- 姓名：叶攀
- 班级：1



## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。



## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。



## 实验步奏

1. 第一步：以system登录到pdborcl，创建角色con_res_role和用户sale，并授权和分配空间：

   ~~~sh
   [oracle@oracle ~]$ sqlplus system/123@oracle:1521/pdb
   
   SQL*Plus: Release 19.0.0.0.0 - Production on Tue Apr 18 09:19:58 2023
   Version 19.3.0.0.0
   
   Copyright (c) 1982, 2019, Oracle.  All rights reserved.
   
   Last Successful login time: Tue Apr 18 2023 09:19:23 +08:00
   
   Connected to:
   Oracle Database 19c Enterprise Edition Release 19.0.0.0.0 - Production
   Version 19.3.0.0.0
   # 检查是否登录到system
   SQL> show user;
   USER is "SYSTEM"
   # 检查是否连接到PDB数据库
   SQL> show con_name;
   
   CON_NAME
   ------------------------------
   PDB
   ~~~

   创建新角色

   ~~~sh
   SQL> create role con_res_role;
   
   Role created.
   
   SQL> grant user sale identified by 123 default tablespace users temporary tablespace temp;
   grant user sale identified by 123 default tablespace users temporary tablespace temp
         *
   ERROR at line 1:
   ORA-00990: missing or invalid privilege
   
   
   SQL> GRANT connect,resource,CREATE VIEW TO con_res_role;
   
   Grant succeeded.
   
   SQL> CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
   
   User created.
   
   SQL> ALTER USER sale default TABLESPACE "USERS";
   
   User altered.
   
   SQL> ALTER USER sale QUOTA 50M ON users;
   
   User altered.
   
   SQL> GRANT con_res_role TO sale;
   
   Grant succeeded.
   ~~~

   **补充**：

   授权`SALE`用户访问`users`表空间，空间限额为50M.

   ![第一步](./1.jpg)

2. 新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

   ~~~sh
   # 登录到PDB数据库
   [oracle@oracle ~]$ sqlplus sale/123@oracle:1521/pdb
   
   SQL*Plus: Release 19.0.0.0.0 - Production on Tue Apr 18 09:42:20 2023
   Version 19.3.0.0.0
   
   Copyright (c) 1982, 2019, Oracle.  All rights reserved.
   
   
   Connected to:
   Oracle Database 19c Enterprise Edition Release 19.0.0.0.0 - Production
   Version 19.3.0.0.0
   
   SQL> show user;
   USER is "SALE"
   SQL> show con_name;
   
   CON_NAME
   ------------------------------
   PDB
   ~~~

   ~~~sh
   SQL> select * from session_privs;
   
   PRIVILEGE
   ----------------------------------------
   CREATE SESSION
   CREATE TABLE
   CREATE CLUSTER
   CREATE VIEW
   CREATE SEQUENCE
   CREATE PROCEDURE
   CREATE TRIGGER
   CREATE TYPE
   CREATE OPERATOR
   CREATE INDEXTYPE
   SET CONTAINER
   
   11 rows selected.
   
   SQL> select * from session_roles;
   
   ROLE
   --------------------------------------------------------
   CON_RES_ROLE
   CONNECT
   RESOURCE
   SODA_APP
   
   SQL> CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
   
   Table created.
   
   SQL> INSERT INTO customers(id,name)VALUES(1,'zhang');
   
   1 row created.
   
   SQL> INSERT INTO customers(id,name)VALUES (2,'wang');
   
   1 row created.
   
   SQL> CREATE VIEW customers_view AS SELECT name FROM customers;
   
   View created.
   
   SQL> GRANT SELECT ON customers_view TO hr;
   
   Grant succeeded.
   
   SQL> SELECT * FROM customers_view;
   
   NAME
   --------------------------------------------------
   zhang
   wang
   ~~~

   ![第2步](./2.jpg)

3. 用户hr连接到pdborcl，查询sale授予它的视图`customers_view`：

   ~~~sh
   [oracle@oracle ~]$ sqlplus hr/123@oracle:1521/pdb
   
   SQL*Plus: Release 19.0.0.0.0 - Production on Tue Apr 18 09:56:20 2023
   Version 19.3.0.0.0
   
   Copyright (c) 1982, 2019, Oracle.  All rights reserved.
   
   Last Successful login time: Tue Apr 11 2023 11:16:26 +08:00
   
   Connected to:
   Oracle Database 19c Enterprise Edition Release 19.0.0.0.0 - Production
   Version 19.3.0.0.0
   
   SQL> show user;
   USER is "HR"
   SQL> show con_name;
   
   CON_NAME
   ------------------------------
   PDB
   
   SQL> SELECT * FROM sale.customers_view;
   
   NAME
   --------------------------------------------------
   zhang
   wang
   ~~~

   ![第3步](./3.jpg)

### 概要文件设置

将用户登录的尝试次数设置为3，尝试次数超过3次之后将会锁住用户。

~~~sh
[oracle@oracle ~]$ sqlplus system/123

SQL*Plus: Release 19.0.0.0.0 - Production on Tue Apr 18 10:03:19 2023
Version 19.3.0.0.0

Copyright (c) 1982, 2019, Oracle.  All rights reserved.

Last Successful login time: Tue Apr 18 2023 09:19:58 +08:00

Connected to:
Oracle Database 19c Enterprise Edition Release 19.0.0.0.0 - Production
Version 19.3.0.0.0

SQL> show user;
USER is "SYSTEM"
SQL> show con_name;

CON_NAME
------------------------------
PDB
SQL> ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;

Profile altered.
~~~

![第4步](./4.jpg)

**试错展示**：

~~~sh
[oracle@oracle ~]$ sqlplus hr/456@oracle:1521/pdb

SQL*Plus: Release 19.0.0.0.0 - Production on Sat Apr 22 17:24:02 2023
Version 19.3.0.0.0

Copyright (c) 1982, 2019, Oracle.  All rights reserved.

ERROR:
ORA-01017: 用户名/口令无效; 登录被拒绝


Enter user-name: hr
Enter password: 
ERROR:
ORA-01017: 用户名/口令无效; 登录被拒绝


Enter user-name: hr
Enter password: 
ERROR:
ORA-01017: 用户名/口令无效; 登录被拒绝


SP2-0157: unable to CONNECT to ORACLE after 3 attempts, exiting SQL*Plus
[oracle@oracle ~]$ sqlplus hr/456@oracle:1521/pdb

SQL*Plus: Release 19.0.0.0.0 - Production on Sat Apr 22 17:24:19 2023
Version 19.3.0.0.0

Copyright (c) 1982, 2019, Oracle.  All rights reserved.

ERROR:
ORA-01017: 用户名/口令无效; 登录被拒绝


Enter user-name: hr
Enter password: 
ERROR:
ORA-01017: 用户名/口令无效; 登录被拒绝


Enter user-name: hr
Enter password: 
ERROR:
ORA-01017: 用户名/口令无效; 登录被拒绝


SP2-0157: unable to CONNECT to ORACLE after 3 attempts, exiting SQL*Plus
[oracle@oracle ~]$ sqlplus hr/123@oracle:1521/pdb

SQL*Plus: Release 19.0.0.0.0 - Production on Sat Apr 22 17:24:43 2023
Version 19.3.0.0.0

Copyright (c) 1982, 2019, Oracle.  All rights reserved.

ERROR:
ORA-28000: 帐户已锁定。
~~~

结果上我们发现已经`hr`用户已经被锁定，登录`system`用户为其解锁：

~~~sh
[oracle@oracle ~]$ sqlplus system/123@oracle:1521/pdb

SQL> alter user sale  account unlock;

User altered.

SQL> alter user hr account unlock;

User altered.
~~~

在此尝试登录，检测是否解锁：

~~~sh
[oracle@oracle ~]$ sqlplus hr/123@oracle:1521/pdb

SQL*Plus: Release 19.0.0.0.0 - Production on Sat Apr 22 17:27:46 2023
Version 19.3.0.0.0

Copyright (c) 1982, 2019, Oracle.  All rights reserved.

Last Successful login time: Tue Apr 18 2023 09:56:20 +08:00

Connected to:
Oracle Database 19c Enterprise Edition Release 19.0.0.0.0 - Production
Version 19.3.0.0.0

SQL> show con_name

CON_NAME
------------------------------
PDB
SQL> show user
USER is "HR"
~~~

现在完全可以登录，并且用户也是`hr`用户。

![第5步](./5.jpg)

### 查看数据库的使用情况

~~~sh
[oracle@oracle ~]$ sqlplus system/123@oracle:1521/pdb

SQL*Plus: Release 19.0.0.0.0 - Production on Sat Apr 22 17:28:40 2023
Version 19.3.0.0.0

Copyright (c) 1982, 2019, Oracle.  All rights reserved.

Last Successful login time: Sat Apr 22 2023 17:25:50 +08:00

Connected to:
Oracle Database 19c Enterprise Edition Release 19.0.0.0.0 - Production
Version 19.3.0.0.0

SQL> SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

TABLESPACE_NAME
------------------------------
FILE_NAME
--------------------------------------------------------------------------------
	MB     MAX_MB AUT
---------- ---------- ---
USERS
/u01/app/oracle/oradata/ORCL/pdb/users01.dbf
    1287.5 32767.9844 YES
~~~

~~~sh
SQL> SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
  2    3    4    5    6    7    8  
表空间名                           大小MB     剩余MB     使用MB    使用率%
------------------------------ ---------- ---------- ---------- ----------
SYSTEM				      210    20.6875   189.3125      90.15
UNDOTBS1			     2500     2246.5	  253.5      10.14
SYSAUX				      165      22.75	 142.25      86.21
USERS				   1287.5     61.875   1225.625      95.19
~~~

![第6步](./6.jpg)

### 实验结束

实验结束，删除用户与角色

~~~sh
[oracle@oracle ~]$ sqlplus system/123@oracle:1521/pdb

SQL> drop role con_res_role;

Role dropped.

SQL> drop user sale cascade;

User dropped.
~~~

![第7步](./7.jpg)



## 结论

在Oracle数据库中，我们可以通过创建角色和用户来管理权限。本实验中，我们创建了一个新的本地角色con_res_role，该角色包含connect和resource角色，并且也包含CREATE VIEW权限。然后我们创建了一个名为sale的用户，并将其授予con_res_role角色。

在测试阶段，我们使用sale用户连接数据库，并创建了一张表并插入数据。接着我们创建了一张视图并查询了表和视图的数据。

通过这个实验，我们可以得出以下结论：

1. 创建角色可以方便地对多个权限进行管理。
2. 将角色授权给用户可以快速地分配权限。
3. 在测试阶段，我们成功地使用新用户sale进行了表和视图的操作，说明该用户拥有所需的权限。

需要注意的是，在实际应用中，应该根据具体情况来分配不同的权限，以保证数据库安全性。

在Oracle中，通过角色和权限的管理，我们可以很好地控制用户对数据库的访问和操作。同时，对于大型数据库来说，优化工具也是非常重要的，可以有效提高数据库的性能和稳定性。

